import { TodoRouter } from "./Routers/Todo/TodoRouter";
import { IRouterClass } from "./Interfaces/IRouterClass";
import { Araam } from "./Araam";
import { ITodoProvider } from "./Routers/Todo/ITodoProvider";
import { Logger } from "./Utils/Logger";

class TodoProvider implements ITodoProvider { }
Logger.toggleWarning();
// Logger.toggleParams();
// Logger.toggleInfo();
Logger.toggleError();
Logger.methodCallLogEnabled = false;
const araamApp = new Araam(require("./Routers/AppConfig.json"));

const instanceMap = new Map<string, IRouterClass>([
	["Todo", new TodoRouter(new TodoProvider())]
]);

araamApp.instanceMap = instanceMap;

const result = araamApp.run()
