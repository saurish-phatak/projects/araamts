import path from "path";
import { IAraam } from "../Interfaces/IAraam";

export class AraamFactory {
	public static getInstance(className: string): IAraam {
		if (AraamFactory.araamClassMap.has(className)) {
			const araamInstance = require(path.resolve(AraamFactory.araamClassMap.get(className) as string));

			return new araamInstance.default();
		}

		throw new Error(`No implementation for ${className} exists.`);
	}

	protected static araamClassMap = new Map<string, string>([
		["araam", "./typescript/AraamImplementations/AraamExtended.ts"]
	]);
}