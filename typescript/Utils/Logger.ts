import { LoggerBase } from "./LoggerBase";

export class Logger extends LoggerBase {
	// If method calls are to be logged
	public static methodCallLogEnabled: boolean = true;

	// Override call()
	public static call() {
		// If the method calls are to be logged, 
		// call the super's call() and return
		if (this.methodCallLogEnabled == true) {
			return super.call();
		}

		// Else return a decorator as it is
		return (
			target: any,
			propertyKey: string,
			propertyDescriptor: PropertyDescriptor
		) => {
			return propertyDescriptor;
		}
	}

	public static indent(str: string, level: number, fillString: string = "  ") {
		return str.padStart(str.length + level * fillString.length, fillString);
	}
}