import { IRouteConfig } from "./IRouteConfig";

export interface IRouteConfigExtended extends IRouteConfig {
	methodName: string;
}