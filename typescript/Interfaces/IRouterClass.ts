import express from "express";
import { RouteCallback } from "../Types/RouteCallback";

export interface IRouterClass {
	routeMethods: Map<string, RouteCallback>;

	_router: express.Router;

}