import { IRouterConfig } from "../Interfaces/IRouterConfig";
import { IRouteConfigExtended } from "./IRouteConfigExtended";

export interface IRouterConfigExtended extends IRouterConfig {
	routes: IRouteConfigExtended[];
}