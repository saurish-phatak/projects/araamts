import { IAppConfig } from "./IAppConfig";
import { IRouterConfigExtended } from "./IRouterConfigExtended";

export interface IAppConfigExtended extends IAppConfig {
	routers: IRouterConfigExtended[];

}