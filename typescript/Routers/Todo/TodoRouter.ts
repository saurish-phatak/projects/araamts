import { Router, Request, Response, NextFunction } from "express";
import { IRouterClass } from "../../Interfaces/IRouterClass";
import { ITodoProvider } from "./ITodoProvider";
import { RouteCallback } from "../../Types/RouteCallback";

export class TodoRouter implements IRouterClass {
	protected todo = { id: 1, text: "Test", completed: false };
	routeMethods: Map<string, RouteCallback> = new Map<string, RouteCallback>();
	_router!: Router;

	public constructor(protected todoProvider: ITodoProvider) {
		this.routeMethods.set("getAllTodos", this.getAllTodos.bind(this));
		this.routeMethods.set("addTodo", this.addTodo.bind(this));
	}

	public getAllTodos(req: Request, res: Response, nextFunction: NextFunction) {
		res.send(this.todo);
	}

	public addTodo(req: Request, res: Response, nextFunction: NextFunction) {
		res.send(this.todo);
	}
}
