import { NextFunction, Response, Request } from "express";

export type RouteCallback = (request: Request, response: Response, nextFunction: NextFunction) => void;