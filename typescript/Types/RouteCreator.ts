import { NextFunction, Response, Request } from "express";
import { RouteCallback } from "./RouteCallback";

export type RouteAdder = (router: any, url: string, callbackMethod: RouteCallback) => void;