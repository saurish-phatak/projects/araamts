import { IAppConfigExtended } from "../Interfaces/IAppConfigExtended";
import { RouteCallback } from "../Types/RouteCallback";
import express, { Router } from "express";
import path from "path";
import { IRouterConfigExtended } from "../Interfaces/IRouterConfigExtended";
import { IRouterClass } from "../Interfaces/IRouterClass";
import { IAraam } from "../Interfaces/IAraam";
import AraamBase from "./AraamBase";
import { RouteAdder } from "../Types/RouteCreator";
import { Logger } from "../Utils/Logger";
import chalk from "chalk";

@Logger.log
export default class AraamExtended extends AraamBase implements IAraam {
	protected _appConfig!: IAppConfigExtended;
	public instanceMap: Map<string, IRouterClass> = new Map<string, IRouterClass>();
	protected httpVerbTypeMap: Map<string, RouteAdder>;

	public constructor() {
		super();

		Logger.info(`Araam instance created.`, []);

		this.httpVerbTypeMap = new Map<string, RouteAdder>();

		// Populate the httpVerbTypeMap
		this.httpVerbTypeMap.set("GET", this.GETVerbAdder.bind(this));
		this.httpVerbTypeMap.set("POST", this.POSTVerbAdder.bind(this));
		this.httpVerbTypeMap.set("DELETE", this.DELETEVerbAdder.bind(this));
		this.httpVerbTypeMap.set("PUT", this.PUTVerbAdder.bind(this));


		// Add new error strings to the error map
		this.errorMap.set("routes", `No routes found for this router. Skipping it : `);
		this.errorMap.set("loadCallback", `Could not load the callback method. Skipping this route : `);
		this.errorMap.set("verbAdder", `Route "type" not one of [GET, POST, PUT, DELETE]. Skipping this route : `);
		this.errorMap.set("url", `Route "url" not specified. Skipping this route : `);
		this.errorMap.set("mountPoint", `Route "mountPoint" not specified. Generating default mountPoint :`);

	}

	public set appConfig(appConfig: IAppConfigExtended) {
		this._appConfig = appConfig;
	}

	public set routerInstanceMap(instanceMap: Map<string, IRouterClass>) {
		this.instanceMap = instanceMap;
	}

	@Logger.call()
	public run() {
		Logger.info(chalk.magenta(`Araam setup start.`), []);

		// Add routers 
		this.addRouters();

		// Start the app on the specified port
		try {
			if (this._appConfig?.port > 0) {
				this.app.listen(this._appConfig.port, () => { Logger.info(chalk.green(`${this._appConfig.appName ?? "App"} started on [PORT :${chalk.white(this._appConfig.port)}]`), []) });
				Logger.info(chalk.magenta(`Araam setup end.`), []);

			}
			else {
				Logger.error(`Valid port number not specified. Exiting Araam. [PORT : ${chalk.white(this._appConfig.port)}]`, []);
			}
		} catch (exception) {
			Logger.error(exception as string, []);
		}

		return this.app;
	}

	@Logger.call()
	protected addRouters() {
		Logger.info(Logger.indent(`Trying to setup up routers.`, 1), []);

		if (this._appConfig.routers?.length > 0) {
			Logger.info(Logger.indent(`Routers found in config file.`, 2), []);

			// For each router
			this._appConfig.routers.forEach(routerObject => {
				// Holds the new Router
				const expressRouter = express.Router();

				// Add the routes now
				this.addRoutes(expressRouter, routerObject);

				// Mount the router onto the app
				if (routerObject.mountPoint?.length > 0) {
					Logger.info(Logger.indent(`${chalk.magenta("Mounting Router ::")} ${chalk.white("[Router Mountpoint : " + routerObject.mountPoint + "]")}` + '\n\n', 3, chalk.magenta("--")), []);
					this.app.use(routerObject.mountPoint, expressRouter);
				}

				else {
					if (routerObject.routerName?.length > 0) {
						const defaultMountPoint = `/${routerObject.routerName.toLowerCase()}`;
						Logger.warn(Logger.indent(`${this.errorMap.get("mountPoint")} ${defaultMountPoint}`, 3));

						// Mount the router on the default mount point
						Logger.info(Logger.indent(`${chalk.magenta("Mounting Router ::")} ${chalk.white("[Router Mountpoint : " + defaultMountPoint + "]")}` + '\n\n', 3, chalk.magenta("--")), []);
						this.app.use(defaultMountPoint, expressRouter);
					}

					else {
						// Skip mounting the router
						Logger.error(Logger.indent(`${this.errorMap.get("routerNameAndMountPoint")} ${JSON.stringify(routerObject)}`, 3), []);
					}
				}
			});
		} else {
			Logger.error(Logger.indent(`No routers found for this app. Skipping setup.`, 1), []);
		}
	}

	@Logger.call()
	protected addRoutes(expressRouter: express.Router, routerConfig: IRouterConfigExtended) {
		Logger.info(Logger.indent(`${chalk.magenta("Adding routes to :: ")}${chalk.white("[Router Name : " + routerConfig.routerName + "]")}`, 3, chalk.magenta("--")), []);

		// If the routerName exists, try to get the RouterClass instance 
		// for that routerName
		let instance = this.instanceMap.get(routerConfig.routerName);

		// If there's no instance, try the normal router setup 
		if (!instance) {
			Logger.warn(Logger.indent(`No RouterClass instance for :: ${chalk.white("[Router Name : " + routerConfig.routerName + "]")}`, 4));
			Logger.warn(Logger.indent(`Hope router has "methodModules" :: ${chalk.white("[Router Name : " + routerConfig.routerName + "]")}`, 4));

			// Setup router normally
			this.addModuleMethodRoute(expressRouter, routerConfig);
			return;
		}

		if (instance) {
			Logger.info(Logger.indent(`RouterClass instance found :: ${chalk.white("[Router Name : " + routerConfig.routerName + "]")}`, 4), []);

			this.addRouterClassMethodRoute(expressRouter, routerConfig, instance as IRouterClass);

			// Set the router in the instance
			instance._router = expressRouter;
			return;
		}

		Logger.error(Logger.indent(`Router "className" not found. Skipping this router. ${chalk.white("[Router Name : " + routerConfig.routerName + "]")}`, 3), []);

	}

	@Logger.call()
	protected GETVerbAdder(expressRouter: express.Router, url: string, callbackMethod: RouteCallback) {
		if (url?.length > 0) {
			Logger.info(Logger.indent(`Adding GET route :: ${chalk.white("[Route URL : " + url + "]")}`, 5), []);
			expressRouter.get(url, callbackMethod);
			return;
		}

		Logger.error(Logger.indent(`${this.errorMap.get("url")} ${chalk.white("[Method Name : " + callbackMethod.prototype?.name + "]")}`, 5), []);
	}

	@Logger.call()
	protected DELETEVerbAdder(expressRouter: express.Router, url: string, callbackMethod: RouteCallback) {
		if (url?.length > 0) {
			Logger.info(Logger.indent(`Adding DELETE route :: ${chalk.white("[Route URL : " + url + "]")}`, 5), []);
			expressRouter.delete(url, callbackMethod);
			return;
		}

		Logger.error(Logger.indent(`${this.errorMap.get("url")} ${chalk.white("[Method Name : " + callbackMethod.prototype?.name + "]")}`, 5), []);
	}

	@Logger.call()
	protected PUTVerbAdder(expressRouter: express.Router, url: string, callbackMethod: RouteCallback) {
		if (url?.length > 0) {
			Logger.info(Logger.indent(`Adding PUT route :: ${chalk.white("[Route URL : " + url + "]")}`, 5), []);
			expressRouter.put(url, callbackMethod);
			return;
		}

		Logger.error(Logger.indent(`${this.errorMap.get("url")} ${chalk.white("[Method Name : " + callbackMethod.prototype?.name + "]")}`, 5), []);
	}

	@Logger.call()
	protected POSTVerbAdder(expressRouter: express.Router, url: string, callbackMethod: RouteCallback) {
		if (url?.length > 0) {
			Logger.info(Logger.indent(`Adding POST route :: ${chalk.white("[Route URL : " + url + "]")}`, 5), []);
			expressRouter.post(url, callbackMethod);
			return;
		}

		Logger.error(Logger.indent(`${this.errorMap.get("url")} ${chalk.white("[Method Name : " + callbackMethod.prototype?.name + "]")}`, 5), []);
	}

	@Logger.call()
	protected addRouterClassMethodRoute(expressRouter: Router, routerConfig: IRouterConfigExtended, instance: IRouterClass) {
		// If the routes property is a populated array
		if (routerConfig.routes?.length > 0) {

			routerConfig.routes.forEach(route => {
				Logger.info(Logger.indent(`Loading :: ${chalk.white("[Method Name : " + route.methodName + "]")}`, 5), []);

				// Get the route method from the instance
				let callback = instance.routeMethods?.get(route.methodName) as RouteCallback;

				if (!callback) {
					Logger.error(Logger.indent(`${this.errorMap.get("loadCallback")} ${chalk.white("[Method Name : " + route.methodName + "]")}`, 5), []);
					return;
				}

				// Get the callback adder method
				if (route.type) {
					//! Dirty trick
					callback.prototype = { name: route.methodName };

					const callbackAdder = this.httpVerbTypeMap.get(route.type);

					// Invoke the callbackAdder
					callbackAdder ? callbackAdder(expressRouter, route.url, callback) : Logger.error(Logger.indent(`${this.errorMap.get("verbAdder")} ${chalk.white("[Method Name : " + route.methodName + "]")}`, 5), []);
				}

				else {
					Logger.error(Logger.indent(`${this.errorMap.get("type")} ${chalk.white("[Method Name : " + route.methodName + "]")}`, 5), []);
				}

			});
		} else {
			Logger.error(Logger.indent(`${this.errorMap.get("routes")} ${chalk.white("[Router Name : " + routerConfig.routerName + "]")}`, 3), []);
		}
	}

	@Logger.call()
	protected addModuleMethodRoute(expressRouter: Router, routerConfig: IRouterConfigExtended) {
		if (routerConfig.routes?.length > 0) {
			// For each route in the routerConfig
			routerConfig.routes.forEach(route => {
				if (route.methodModule?.length > 0) {
					try {
						Logger.info(Logger.indent(`Loading :: ${chalk.white("[Method Module : " + route.methodModule + "]")}`, 5), []);

						// Load the method from the module
						let callback: RouteCallback = require(path.resolve(route.methodModule));

						// Get the routeAdder method for the router
						if (route.type?.length > 0) {
							let routeAdder = this.httpVerbTypeMap.get(route.type) as RouteAdder;

							// If the routeAdder exits
							routeAdder ? routeAdder(expressRouter, route.url, callback) : Logger.error(Logger.indent(`${this.errorMap.get('verbAdder')} ${chalk.white("[Method Module : " + route.methodModule + "]")}`, 5), []);
						}
						else {
							Logger.error(Logger.indent(`${this.errorMap.get("type")} ${chalk.white("[Method Module : " + route.methodModule + "]")}`, 5), []);
						}
					} catch (exception) {
						Logger.error(Logger.indent(`${this.errorMap.get('loadCallback')} ${chalk.white("[Method Module : " + route.methodModule + "]")}`, 5), []);
						return;
					}
				}
				else {
					Logger.error(Logger.indent(`${this.errorMap.get("methodModule")} ${chalk.white(JSON.stringify(route))}`, 5), []);
				}
			});

		} else {
			Logger.error(Logger.indent(`${this.errorMap.get("routes")} ${chalk.white("[Router Name : " + routerConfig.routerName + "]")}`, 3), []);
		}

	}
}


