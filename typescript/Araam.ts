import { IAppConfigExtended } from "./Interfaces/IAppConfigExtended";
import { AraamFactory } from "./Utils/AraamFactory";
import { IAraam } from "./Interfaces/IAraam";
import { IRouterClass } from "./Interfaces/IRouterClass";

export class Araam {
	protected _AraamImplementation: IAraam;
	protected _instanceMap!: Map<string, IRouterClass>;

	public constructor(public appConfig: IAppConfigExtended) {
		// Load the instance
		this._AraamImplementation = AraamFactory.getInstance("araam");

		// Set the appConfig
		this._AraamImplementation.appConfig = appConfig;
	}

	public run() {
		this._AraamImplementation.run();
	}

	public set instanceMap(instanceMap: Map<string, IRouterClass>) {

		this._instanceMap = instanceMap;

		// Set the instance map in the araam instance
		this._AraamImplementation.instanceMap = instanceMap;

		// console.log(this._AraamImplementation);
	}

	public get instanceMap() {
		return this._instanceMap;
	}
}