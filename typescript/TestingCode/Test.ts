const indent = (str: string, level: number, fillString: string = "   ") => {
	console.log(str.padStart(str.length + level * fillString.length, fillString));
	console.log(str.padStart(str.length + level * " ".length, " "));

}

console.log(indent("Adding Get route", 5));
